# README #

Structured Data Schema Markup using global Shopify objects to increase organic traffic. Most of the schema is dynamically generated. Client specific fields will need to be populated manually, such as addressLocality or geoLocation. The schema can be more gradual using custom Shopify objects on a per client basis. This package gives a solid schema markup out of the box with little customization. 

https://search.google.com/structured-data/testing-tool/u/0/ for testing.

### Setup  

* When testing on staging themes, view & copy the page source & paste into code snippet to test schema.

* Include the json.liquid files inside snippets/

* Add to theme.liquid in <head> after <title>
    
  <!---JSON SCHEMA MARKUP--->
    {% if template == 'index' %}
      {% include 'organization-schema' %}
      {% include 'store-schema' %}
    {% endif %}
    {% include 'website-schema' %}
    {% include 'site-nav-element-schema' %}
    {% include 'breadcrumb-schema' %}
  <!--END MARKUP-->


* In article-template.liquid add {% include 'article-json' %}

* In product-template.liquid add {% include 'product-json' %}

